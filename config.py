import pandas as pd

preference_scores_of_participant = ['attractive_important', 'sincere_important', 'intelligence_important', 'funny_important', 'ambition_important', 'shared_interests_important']
preference_scores_of_partner = ['pref_o_attractive', 'pref_o_sincere', 'pref_o_intelligence', 'pref_o_funny', 'pref_o_ambitious', 'pref_o_shared_interests']
rating_of_partner_from_participant = ['attractive_partner', 'sincere_partner', 'intelligence_parter', 'funny_partner', 'ambition_partner', 'shared_interests_partner']
data = pd.read_csv('dating-full.csv')
n = len(data)
cols = data.keys()
continuous_valued_columns = list(cols)
continuous_valued_columns.remove('gender')
continuous_valued_columns.remove('race')
continuous_valued_columns.remove('race_o')
continuous_valued_columns.remove('samerace')
continuous_valued_columns.remove('field')
continuous_valued_columns.remove('decision')    
del data, cols, n
