import pandas as pd
import numpy as np
import copy
import sys
import matplotlib.pyplot as plt
from trees import Node, DecisionTree, BaggingTrees, RandomForest, AdaBoost

class kFoldValidation:
    def __init__(self, k, trainset, config, t_frac=1):
        self.k = k
        self.trainset = trainset
        self.models = dict()
        self.config = config
        self.validationset = dict()
        self.trainset_k = dict()
        self.t_frac = t_frac
        self.createsets()
        self.createmodels()
    
    def partition(self, n, k):
        m = int(n/k)
        ind = dict()
        for i in range(k-1):
            ind[i] = []
            for j in range(m):
                ind[i].append(i*m+j)
        ind[k-1] = []
        j = (k-1)*m
        while j < n:
            ind[k-1].append(j)
            j += 1
        return ind
        
    def createsets(self):
        ind = self.partition(len(self.trainset), self.k)
        whole = list(range(len(self.trainset)))
        for i in range(self.k):
            self.validationset[i] = self.trainset.iloc[ind[i]][:]
            res = list(set(whole).difference(set(ind[i])))
            self.trainset_k[i] = self.trainset.iloc[res][:]
            self.trainset_k[i] = self.trainset_k[i].sample(frac=self.t_frac, random_state=32)
            self.trainset_k[i].reset_index(drop=True, inplace=True)
        self.fraction = len(self.trainset_k[0])
        #print(self.t_frac, self.trainset_k[0])
         
    def createmodels(self):
        if self.config['model'] == 'DT':
            for i in range(self.k):
                self.models[i] = DecisionTree(self.trainset_k[i], self.config['label'], self.config['max_depth'], self.config['min_exp'])
                self.models[i].fit()
        elif self.config['model'] == 'BT':
            for i in range(self.k):
                self.models[i] = BaggingTrees(self.config['num_trees'], self.trainset_k[i], self.config['label'], self.config['frac'], self.config['max_depth'], self.config['min_exp'])
        elif self.config['model'] == 'RF':
            for i in range(self.k):
                self.models[i] = RandomForest(self.config['num_trees'], self.trainset_k[i], self.config['label'], self.config['frac'], self.config['max_depth'], self.config['min_exp'])
        elif self.config['model'] == 'AB':
            for i in range(self.k):
                self.models[i] = AdaBoost(self.config['T'], self.trainset_k[i], self.config)
                
    def crossvalidation(self):
        self.accu = []
        for i in range(self.k):
            pred = []
            for j in range(len(self.validationset[i])):
                pred.append(self.models[i].predict(self.validationset[i].iloc[j][:]))
            accu = self.models[i].accuracy(pred, list(self.validationset[i].loc[:, self.config['label']]))
            self.accu.append(accu)
        self.accu_avg = np.mean(self.accu)
        self.stderr = self.standarderror(self.accu)
    
    def standarderror(self, lst):
        mean = np.mean(lst)
        var = 0.0
        for i in range(len(lst)):
            var += np.square(lst[i]-mean)/len(lst)
        var = np.sqrt(var)
        return var/np.sqrt(len(lst))
    
def main():
    trainingSet = sys.argv[1]
    #print(trainingSet)
    trainset = pd.read_csv(trainingSet)
    trainset = trainset.sample(frac = 1, random_state = 18)
    trainset = trainset.sample(frac = 0.5, random_state = 32)
    #print(trainset)
    label = 'decision'
    max_depths = [3,5,7,9]
    min_exp = 50
    num_trees = 30
    frac = 1
    k = 10
    T = 30
    accu_avg_dt = []
    accu_avg_bt = []
    accu_avg_rf = []
    accu_avg_ab = []
    stderr_dt = []
    stderr_bt = []
    stderr_rf = []
    stderr_ab = []
    config_dt = {'model': 'DT', 'label': label, 'max_depth': 8, 'min_exp': min_exp, 'frac': 1, 'num_trees': 30}
    config_bt = {'model': 'BT', 'label': label, 'max_depth': 8, 'min_exp': min_exp, 'frac': 1, 'num_trees': 30}
    config_rf = {'model': 'RF', 'label': label, 'max_depth': 8, 'min_exp': min_exp, 'frac': 1, 'num_trees': 30}
    config_ab = {'model': 'AB', 'label': label, 'max_depth': 8, 'min_exp': min_exp, 'frac': 1, 'num_trees': 30, 'T': T}
    for i in range(len(max_depths)):
        config_dt['max_depth'] = max_depths[i]
        config_bt['max_depth'] = max_depths[i]
        config_rf['max_depth'] = max_depths[i]
        DT = kFoldValidation(k, trainset, config_dt)
        BT = kFoldValidation(k, trainset, config_bt)
        RF = kFoldValidation(k, trainset, config_rf)
        DT.crossvalidation()
        BT.crossvalidation()
        RF.crossvalidation()
        accu_avg_dt.append(DT.accu_avg)
        stderr_dt.append(DT.stderr)
        accu_avg_bt.append(BT.accu_avg)
        stderr_bt.append(BT.stderr)
        accu_avg_rf.append(RF.accu_avg)
        stderr_rf.append(RF.stderr)
    fig = plt.figure()
    #print(len(sizes), len(accu_lr_mean), len(stderr_lr))
    plt.errorbar(max_depths, accu_avg_dt, yerr=stderr_dt, label='Decision Tree')
    plt.errorbar(max_depths, accu_avg_bt, yerr=stderr_bt, label='Bagging Trees')
    plt.errorbar(max_depths, accu_avg_rf, yerr=stderr_rf, label='Random Forest')
    plt.xlabel('Depth limit')
    plt.ylabel('Average accuracy')
    plt.legend(loc='upper right')
    plt.savefig('depth.png')
    plt.show()
    
    diff = accu_avg_rf - accu_avg_bt
    #diff = np.array([0.09807692, 0.08673077, 0.08576923, 0.10269231, 0.0725, 0.10538462, 0.09519231, 0.09269231, 0.09134615, 0.06826923, 0.07730769])
    print('Difference of the accuracy of Random Forest and that of Bagging Trees:',diff)
    mean_diff = np.mean(diff)
    stddev = 0.0
    for i in range(len(diff)):
        stddev += np.square(mean_diff-diff[i])/(len(diff)-1)
    stddev = np.sqrt(stddev)
    t_stat = mean_diff/(stddev/np.sqrt(len(diff)))
    
    t_dist = scipy.stats.t(len(diff)-1)
    p_value = 2*(1-t_dist.cdf(t_stat))
    print('mean:', mean_diff)
    print('standard deviation:', stddev)
    print('t statistics:', t_stat)
    print('p value:', 2*(1-t_dist.cdf(t_stat)))
    
    alpha = 0.05
    if p_value < alpha:
        print('Null hypothesis can be rejected at significant level', alpha, ', hence the difference between the performance of RF and BT is statistically significant')
    else:
        print('Null hypothesis can not be rejected at significant level', alpha)
    
if __name__ == '__main__':
    main()
