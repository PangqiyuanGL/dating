import pandas as pd
import numpy as np
import copy
import sys
from config import continuous_valued_columns, preference_scores_of_participant, preference_scores_of_partner, rating_of_partner_from_participant

class Node:
    def __init__(self, trainset, att, label):
        self.att = att
        self.set = trainset
        self.children = None
        self.label = label 

class DecisionTree(Node):
    def __init__(self, trainset, label, max_depth, min_exp, downsample=False, sample_weights=None):
        self.trainset = trainset
        self.max_depth = max_depth
        self.min_exp = min_exp
        self.label = label
        self.downsample = downsample
        self.set_sample_weights(sample_weights)
        atts = list(self.trainset.keys())
        atts.remove(self.label)
        atts.remove('sample_weights')
        self.atts = atts
        
    def set_sample_weights(self, sample_weights):
        self.sample_weights = []
        if sample_weights is None:
            self.sample_weights = np.ones(len(self.trainset))
        else:
            self.sample_weights = sample_weights
        self.trainset['sample_weights'] = self.sample_weights
        
    def numofdiffvals(self, series):
        lst = list(series)
        lst.sort()
        pre = lst[0]
        vals = [[pre, 1]]
        count = 1
        num = 1
        for i in range(1, len(lst)):
            if lst[i] != pre:
                count += 1
                pre = lst[i]
                vals.append([pre, 1])
            else:
                vals[-1][1] += 1
        assert count == len(vals)
        assert len(lst) == np.sum([ vals[i][1] for i in range(count)])
        return count, vals  
        
    def probability(self, sets, att):
        vals = dict()
        for i in list(sets.index):
            if sets.loc[i, att] not in list(vals.keys()):
                vals[sets.loc[i, att]] = 0.0
        for i in list(sets.index):
            vals[sets.loc[i, att]] += sets.loc[i, 'sample_weights']
        weights = list(sets.loc[:, 'sample_weights'])
        total = np.sum(weights)
        for key in list(vals.keys()):
            vals[key] /= total
        return vals
        
    def gini(self, sets, att):
        vals = self.probability(sets, att)
        sums = 0.0
        #print(count, vals)
        for i in list(vals.keys()):
            sums += np.square(vals[i])
        return 1 - sums
    
    def indices(self, sets, att, vals):
        ind = dict()
        for i in range(len(vals)):
            ind[vals[i][0]] = []
        for i in list(sets.index):
            for j in range(len(vals)):
                if sets.loc[i,att] == vals[j][0]:
                    ind[vals[j][0]].append(i)
                    break
        return ind
    
    def ginigain(self, sets, att):
        g = self.gini(sets, self.label)
        #print('gini', g, att)
        count, vals = self.numofdiffvals(sets.loc[:, att])
        ind = self.indices(sets, att, vals)
        sums = 0.0
        probs = self.probability(sets, att)
        for i in list(probs.keys()):
            #print(vals[i][1])
            sums += (probs[i])*self.gini(sets.loc[ind[i], :], self.label)
        return g - sums
        
    def maxlabel(self, vals):
        labels = []
        for i in range(len(vals)):
            labels.append(vals[i][1])
        ind = np.argmax(labels)
        label = vals[ind][0]
        #print(vals)
        return label
    
    def recursive(self, sets, atts, depth):
        if depth == self.max_depth or len(sets) <= self.min_exp:
            count, vals = self.numofdiffvals(sets.loc[:, self.label])
            label = self.maxlabel(vals)
            #print('leaf label', label)
            child = Node(None, None, label)
        else:
            if self.downsample:
                maxgg = -10.0
                maxatt = None
                #print(atts)
                ind = np.random.choice(len(self.atts), int(len(self.atts)), replace=False)
                atts_rf = [self.atts[ind[i]] for i in range(len(ind))]
                atts_rf = list(set(atts_rf).difference(set(atts)))
                for att in atts_rf:
                    gg = self.ginigain(sets, att)
                    #print('gini gain', gg)
                    if gg > maxgg:
                        maxgg = gg
                        maxatt = att
                #print(maxgg, maxatt)
                count, labels = self.numofdiffvals(sets.loc[:, self.label])
                label = self.maxlabel(labels)
                #print(label)
                child = Node(sets, maxatt, label)
                count, vals = self.numofdiffvals(sets.loc[:, maxatt])
                ind = self.indices(sets, maxatt, vals)
                child.children = dict()
                newatts = atts_rf
                newatts.remove(maxatt)
                for i in range(len(ind)):
                    child.children[vals[i][0]] = self.recursive(sets.loc[ind[vals[i][0]], :], newatts, depth+1)
            else:
                maxgg = -10.0
                maxatt = None
                #print(atts)
                for att in atts:
                    gg = self.ginigain(sets, att)
                    #print('gini gain', gg)
                    if gg > maxgg:
                        maxgg = gg
                        maxatt = att
                #print(maxgg, maxatt)
                count, labels = self.numofdiffvals(sets.loc[:, self.label])
                label = self.maxlabel(labels)
                #print(label)
                child = Node(sets, maxatt, label)
                count, vals = self.numofdiffvals(sets.loc[:, maxatt])
                ind = self.indices(sets, maxatt, vals)
                child.children = dict()
                for i in range(len(ind)):
                    #print(maxatt, atts)
                    newatts = copy.deepcopy(atts)
                    newatts.remove(maxatt)
                    #print(vals[i])
                    child.children[vals[i][0]] = self.recursive(sets.loc[ind[vals[i][0]], :], newatts, depth+1)
        return child
             
            
    def fit(self):
        if self.downsample:
            self.decisiontree = self.recursive(self.trainset, [], 0)
        else:
            self.decisiontree = self.recursive(self.trainset, self.atts, 0)
    
    def root2leaf(self, node, inputs):
        if node.children is None:
            label = node.label
        else:
            keys = list(node.children.keys())
            #keys.remove('sample_weights')
            #print(inputs[node.att])
            #print(keys)
            if inputs[node.att] not in keys:
                label = node.label
            else:
                label = self.root2leaf(node.children[inputs[node.att]], inputs)
        return label
    
    def predict(self, inputs):
        #print(inputs)
        #assert len(inputs) == 1
        label = self.root2leaf(self.decisiontree, inputs)
        return label 
        
    def accuracy(self, ypred, ytrue):
        n = len(ypred)
        assert n == len(ytrue)
        count = 0
        for i in range(n):
            if ypred[i] == ytrue[i]:
                count += 1
        return 1.0*count/n

class BaggingTrees(DecisionTree):
    def __init__(self, num_trees, trainset, label, frac, max_depth, min_exp, sample_weights=None):
        self.trees = dict()
        self.num_trees = num_trees
        self.label = label
        self.frac = frac
        self.trainset = trainset
        self.max_depth = max_depth
        self.min_exp = min_exp
        self.sample_weights = sample_weights
        if sample_weights:
            self.trainset['sample_weights'] = self.sample_weights
        self.buildtrees()
        
    def buildtrees(self):
        for i in range(self.num_trees):
            set4train = self.trainset.sample(frac=self.frac, random_state=np.random.randint(0, 100), replace=True)
            set4train.reset_index(drop=True, inplace=True)
            if self.sample_weights:
                sample_weights = list(set4train.loc[:, 'sample_weights'])
            else:
                sample_weights = None
            self.trees[i] = DecisionTree(set4train, self.label, self.max_depth, self.min_exp, False, sample_weights)
            self.trees[i].fit()
    
    def majority(self, series):
        lst = list(series)
        lst.sort()
        pre = lst[0]
        vals = [[pre, 1]]
        count = 1
        num = 1
        for i in range(1, len(lst)):
            if lst[i] != pre:
                count += 1
                pre = lst[i]
                vals.append([pre, 1])
            else:
                vals[-1][1] += 1
        assert count == len(vals)
        assert len(lst) == np.sum([ vals[i][1] for i in range(count)])
        votes = [vals[i][1] for i in range(count)]
        ind = np.argmax(votes)
        maj = vals[ind][0]
        return maj
            
    def predict(self, inputs):
        prediction = []
        for j in range(self.num_trees):
           prediction.append(self.trees[j].predict(inputs))
        pred = self.majority(prediction)
        return pred
        
    def accuracy(self, ypred, ytrue):
        n = len(ypred)
        assert n == len(ytrue)
        count = 0
        for i in range(n):
            if ypred[i] == ytrue[i]:
                count += 1
        return 1.0*count/n    
        
class RandomForest(DecisionTree):
    def __init__(self, num_trees, trainset, label, frac, max_depth, min_exp, sample_weights=None):
        self.trees = dict()
        self.num_trees = num_trees
        self.label = label
        self.frac = frac
        self.trainset = trainset
        self.max_depth = max_depth
        self.min_exp = min_exp
        self.sample_weights = sample_weights
        if sample_weights:
            self.trainset['sample_weights'] = self.sample_weights
        self.buildtrees()
        
    def buildtrees(self):
        for i in range(self.num_trees):
            set4train = self.trainset.sample(frac=self.frac, random_state=np.random.randint(0, 100), replace=True)
            set4train.reset_index(drop=True, inplace=True)
            if self.sample_weights:
                sample_weights = list(set4train.loc[:, 'sample_weights'])
            else:
                sample_weights = None
            self.trees[i] = DecisionTree(set4train, self.label, self.max_depth, self.min_exp, True, sample_weights)
            self.trees[i].fit()
    
    def majority(self, series):
        lst = list(series)
        lst.sort()
        pre = lst[0]
        vals = [[pre, 1]]
        count = 1
        num = 1
        for i in range(1, len(lst)):
            if lst[i] != pre:
                count += 1
                pre = lst[i]
                vals.append([pre, 1])
            else:
                vals[-1][1] += 1
        assert count == len(vals)
        assert len(lst) == np.sum([ vals[i][1] for i in range(count)])
        votes = [vals[i][1] for i in range(count)]
        ind = np.argmax(votes)
        maj = vals[ind][0]
        return maj
            
    def predict(self, inputs):
        prediction = []
        for j in range(self.num_trees):
           prediction.append(self.trees[j].predict(inputs))
        pred = self.majority(prediction)
        return pred
        
    def accuracy(self, ypred, ytrue):
        n = len(ypred)
        assert n == len(ytrue)
        count = 0
        for i in range(n):
            if ypred[i] == ytrue[i]:
                count += 1
        return 1.0*count/n

class AdaBoost(DecisionTree):
    def __init__(self, T, trainset, config):
        self.T = T
        self.config = config
        self.trainset = trainset
        self.models = dict()
        self.epsilon = []
        self.alpha = []
        self.sample_weights = [1.0/len(self.trainset)]*len(self.trainset)

    def createmodels(self, t, sample_weights):
        if self.config['model'] == 'DT':
            #for i in range(self.k):
            self.models[t] = DecisionTree(self.trainset, self.config['label'], self.config['max_depth'], self.config['min_exp'], False, sample_weights)
            self.models[t].fit()
        elif self.config['model'] == 'BT':
            #for i in range(self.k):
             self.models[t] = BaggingTrees(self.config['num_trees'], self.trainset, self.config['label'], self.config['frac'], self.config['max_depth'], self.config['min_exp'], sample_weights)
        elif self.config['model'] == 'RF':
            #for i in range(self.k):
             self.models[t] = RandomForest(self.config['num_trees'], self.trainset, self.config['label'], self.config['frac'], self.config['max_depth'], self.config['min_exp'], sample_weights)
             
    def weightederror(self, trainset, pred):
        epsilon = 0.0
        for i in range(len(pred)):
            #print(i, len(pred), pred[i])
            if trainset.iloc[i][self.config['label']] != pred[i]:
                epsilon += trainset.iloc[i]['sample_weights']
        return epsilon         
             
    def new_sample_weights(self, ytrue, ypred, t):
        D = []
        for i in range(len(ytrue)):
            D.append(self.sample_weights[i]*np.exp(-1.0*self.alpha[t]*ytrue[i]*ypred[i]))
        sumD = np.sum(D)
        D /= sumD
        self.sample_weights = D
             
    def fit(self):
        for t in range(self.T):
            self.createmodels(t, self.sample_weights)
            pred = []
            for i in range(len(self.trainset)):
                pred.append(self.models[t].predict(self.trainset.iloc[i][:]))
            self.epsilon.append(self.weightederror(self.models[t].trainset, pred))
            self.alpha.append(1.0/2*np.log((1-self.epsilon[t])/self.epsilon[t]))
            self.new_sample_weights(list(self.trainset.loc[:, self.config['label']]), pred, t)
        
    def predict(self, inputs):
        pred = 0.0
        for t in range(self.T):
            #print(t)
            pred += self.alpha[t]*self.models[t].predict(inputs)
        pred = np.sign(pred)
        if pred == 0:
            pred = -1
        return pred    
        
def decisionTree(trainingSet, testSet):
    trainset = pd.read_csv(trainingSet)
    testset = pd.read_csv(testSet)
    label = 'decision'
    max_depth = 8
    min_exp = 50
    DT = DecisionTree(trainset, label, max_depth, min_exp)
    DT.fit()
    pred = []
    for i in range(len(trainset)):
        pred.append(DT.predict(trainset.iloc[i][:]))
        #print(pred[-1], testset.loc[i, label])
    accu_train = DT.accuracy(pred, list(trainset.loc[:, label]))
    pred = []
    for i in range(len(testset)):
        pred.append(DT.predict(testset.iloc[i][:]))
        #print(pred[-1], testset.loc[i, label])
    accu_test = DT.accuracy(pred, list(testset.loc[:, label]))
    print('Training Accuracy DT:', accu_train)
    print('Testing Accuracy DT:', accu_test)


def bagging(trainingSet, testSet):
    frac = 0.9
    num_trees = 30
    trees = dict()
    trainset = pd.read_csv(trainingSet)
    testset = pd.read_csv(testSet)
    label = 'decision'
    max_depth = 8
    min_exp = 50
    BT = BaggingTrees(num_trees, trainset, label, frac, max_depth, min_exp)
    pred = []
    for i in range(len(trainset)):
        pred.append(BT.predict(trainset.iloc[i][:]))
    accu_train = BT.accuracy(pred, list(trainset.loc[:, label]))
    pred = []
    for i in range(len(testset)):
        pred.append(BT.predict(testset.iloc[i][:]))
    accu_test = BT.accuracy(pred, list(testset.loc[:, label]))
    print('Training Accuracy BT:', accu_train)
    print('Testing Accuracy BT:', accu_test)
    
def randomForest(trainingSet, testSet):
    frac = 0.9
    num_trees = 30
    trees = dict()
    trainset = pd.read_csv(trainingSet)
    testset = pd.read_csv(testSet)
    label = 'decision'
    max_depth = 8
    min_exp = 50
    RF = RandomForest(num_trees, trainset, label, frac, max_depth, min_exp)
    pred = []
    for i in range(len(trainset)):
        pred.append(RF.predict(trainset.iloc[i][:]))
    accu_train = RF.accuracy(pred, list(trainset.loc[:, label]))
    pred = []
    for i in range(len(testset)):
        pred.append(RF.predict(testset.iloc[i][:]))
    accu_test = RF.accuracy(pred, list(testset.loc[:, label]))
    print('Training Accuracy RF:', accu_train)
    print('Testing Accuracy RF:', accu_test)

def zero2negone(sets, label):
    for i in list(sets.index):
        if sets.loc[i, label] == 0:
            sets.loc[i, label] = -1

def adaboost(trainingSet, testSet):
    config = dict()
    T = 20
    config['frac'] = 0.9
    config['num_trees'] = 30
    trees = dict()
    trainset = pd.read_csv(trainingSet)
    testset = pd.read_csv(testSet)
    config['label'] = 'decision'
    config['max_depth'] = 8
    config['min_exp'] = 50
    config['model'] = 'DT'
    zero2negone(trainset, config['label'])
    zero2negone(testset, config['label'])
    AB = AdaBoost(T, trainset, config)
    AB.fit()
    pred = []
    for i in range(len(trainset)):
        pred.append(AB.predict(trainset.iloc[i][:]))
    accu_train = AB.models[0].accuracy(pred, list(trainset.loc[:, config['label']]))
    pred = []
    for i in range(len(testset)):
        pred.append(AB.predict(testset.iloc[i][:]))
    accu_test = AB.models[0].accuracy(pred, list(testset.loc[:, config['label']]))
    print('Training Accuracy AdaBoost:', accu_train)
    print('Testing Accuracy AdaBoost:', accu_test)

def main():
    trainingSet = sys.argv[1]
    testSet = sys.argv[2]
    modelIdx = sys.argv[3]
    if modelIdx == '1':
        decisionTree(trainingSet, testSet)
    elif modelIdx == '2':
        bagging(trainingSet, testSet)
    elif modelIdx == '3':
        randomForest(trainingSet, testSet)
    elif modelIdx == '4':
        adaboost(trainingSet, testSet)

if __name__ == '__main__':
    main() 
