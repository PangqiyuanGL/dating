import pandas as pd
import numpy as np
import copy
import sys
from config import continuous_valued_columns, preference_scores_of_participant, preference_scores_of_partner, rating_of_partner_from_participant

def overflow(data, att, rng):
    count = 0
    for i in list(data.index):
        if data.loc[i, att] > rng[1] or data.loc[i, att] < rng[0]:
            data.loc[i, att] = data.loc[i, att] % rng[1]
            count += 1
    return count

def categr2num(lst):
    n = len(lst)
    #lst.sort()
    num = 0
    ind = sorted(range(n), key=lst.__getitem__)
    maps = {}
    pre = lst[ind[0]]
    maps[lst[ind[0]]] = num
    lst[ind[0]] = num
    
    for i in range(1,n):
        if lst[ind[i]] != pre:
            num += 1
            pre = lst[ind[i]]
            maps[lst[ind[i]]] = num
            lst[ind[i]] = num
        else:
            lst[ind[i]] = num
            
    return lst, maps

def numofdiffvals_sortedlst(lst):
    count = 1
    pre = lst[0]
    vals = [pre]
    for i in range(len(lst)):
        if lst[i] != pre:
            count += 1
            pre = lst[i]
            vals.append(pre)
    assert count == len(vals)
    return count, vals


def onehotvec(data, att):
    lst = list(data.loc[:, att])
    lst.sort()
    count, vals = numofdiffvals_sortedlst(lst)
    mat = np.zeros([len(data), count-1])
    for i in range(len(data)):
        ind = vals.index(lst[i])
        data.loc[i, att] = ind
        if ind != count-1:
            mat[i, ind] = 1.0
    return mat, vals

def dataframe2array(data, onehotcollection):
    mat = data.to_numpy()
    mat = np.append(mat, onehotcollection, axis = 1)
    return mat

def gender2label(data, att):
    for i in range(len(data)):
        if data.loc[i, att] == 'female':
            data.loc[i, att] = 0
        elif data.loc[i, att] == 'male':
            data.loc[i, att] = 1


def main():
    #filepath = sys.argv[0]
    data = pd.read_csv('dating-full.csv')
    data = data.loc[0:6499, :]
    data = data.drop(columns=['race', 'race_o', 'field'])
    n = len(data)
    gender2label(data, 'gender')
    #print(data)
    # normalization
    for i in range(n):
        sums = 0.0
        for att in preference_scores_of_participant:
             sums += data.loc[i, att]
        for att in preference_scores_of_participant:
             data.loc[i, att] = data.loc[i, att]/sums
        sums = 0.0
        for att in preference_scores_of_partner:
             sums += data.loc[i, att]
        for att in preference_scores_of_partner:
             data.loc[i, att] = data.loc[i, att]/sums
    for att in preference_scores_of_participant:
        print('Mean of', att, ':', round(np.mean(data.loc[:, att]), 2))
    for att in preference_scores_of_partner:
        print('Mean of', att, ':', round(np.mean(data.loc[:, att]), 2))

    for att in continuous_valued_columns:
        data.loc[:, att] = pd.cut(data.loc[:, att], bins = 2, labels = [0,1])
    
    testset = data.sample(frac=0.2, random_state=47)
    indices = list(testset.index)
    trainset = data.drop(indices)
    
    testset.to_csv('testSet.csv', index=False)
    trainset.to_csv('trainingSet.csv', index=False)
    
    
    

if __name__ == '__main__':      
    main()
