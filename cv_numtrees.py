import pandas as pd
import numpy as np
import copy
import sys
import matplotlib.pyplot as plt
from trees import Node, DecisionTree, BaggingTrees, RandomForest
from cv_depth import kFoldValidation

def main():
    trainingSet = sys.argv[1]
    #print(trainingSet)
    trainset = pd.read_csv(trainingSet)
    trainset = trainset.sample(frac = 1, random_state = 18)
    trainset = trainset.sample(frac = 0.5, random_state = 32)
    #print(trainset)
    label = 'decision'
    #max_depths = [3,5,7,9]
    min_exp = 50
    num_trees = [10, 20, 40, 50]
    frac = 1
    k = 10
    accu_avg_dt = []
    accu_avg_bt = []
    accu_avg_rf = []
    stderr_dt = []
    stderr_bt = []
    stderr_rf = []
    config_dt = {'model': 'DT', 'label': label, 'max_depth': 8, 'min_exp': min_exp, 'frac': 1, 'num_trees': 30}
    config_bt = {'model': 'BT', 'label': label, 'max_depth': 8, 'min_exp': min_exp, 'frac': 1, 'num_trees': 30}
    config_rf = {'model': 'RF', 'label': label, 'max_depth': 8, 'min_exp': min_exp, 'frac': 1, 'num_trees': 30}
    for i in range(len(num_trees)):
        config_dt['num_trees'] = num_trees[i]
        config_bt['num_trees'] = num_trees[i]
        config_rf['num_trees'] = num_trees[i]
        DT = kFoldValidation(k, trainset, config_dt)
        BT = kFoldValidation(k, trainset, config_bt)
        RF = kFoldValidation(k, trainset, config_rf)
        DT.crossvalidation()
        BT.crossvalidation()
        RF.crossvalidation()
        accu_avg_dt.append(DT.accu_avg)
        stderr_dt.append(DT.stderr)
        accu_avg_bt.append(BT.accu_avg)
        stderr_bt.append(BT.stderr)
        accu_avg_rf.append(RF.accu_avg)
        stderr_rf.append(RF.stderr)
    fig = plt.figure()
    #print(len(sizes), len(accu_lr_mean), len(stderr_lr))
    plt.errorbar(num_trees, accu_avg_dt, yerr=stderr_dt, label='Decision Tree')
    plt.errorbar(num_trees, accu_avg_bt, yerr=stderr_bt, label='Bagging Trees')
    plt.errorbar(num_trees, accu_avg_rf, yerr=stderr_rf, label='Random Forest')
    plt.xlabel('Number of trees')
    plt.ylabel('Average accuracy')
    plt.legend(loc='upper right')
    plt.savefig('numtrees.png')
    plt.show()
    
    diff = accu_avg_rf - accu_avg_bt
    #diff = np.array([0.09807692, 0.08673077, 0.08576923, 0.10269231, 0.0725, 0.10538462, 0.09519231, 0.09269231, 0.09134615, 0.06826923, 0.07730769])
    print('Difference of the accuracy of Random Forest and that of Bagging Trees:',diff)
    mean_diff = np.mean(diff)
    stddev = 0.0
    for i in range(len(diff)):
        stddev += np.square(mean_diff-diff[i])/(len(diff)-1)
    stddev = np.sqrt(stddev)
    t_stat = mean_diff/(stddev/np.sqrt(len(diff)))
    
    t_dist = scipy.stats.t(len(diff)-1)
    p_value = 2*(1-t_dist.cdf(t_stat))
    print('mean:', mean_diff)
    print('standard deviation:', stddev)
    print('t statistics:', t_stat)
    print('p value:', 2*(1-t_dist.cdf(t_stat)))
    
    alpha = 0.05
    if p_value < alpha:
        print('Null hypothesis can be rejected at significant level', alpha, ', hence the difference between the performance of RF and BT is statistically significant')
    else:
        print('Null hypothesis can not be rejected at significant level', alpha)
    
if __name__ == '__main__':
    main()
